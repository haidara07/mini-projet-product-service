const mongoose = require('mongoose');
const Product = require('../models/productModel');

// URL de connexion à la base de données MongoDB
const url = 'mongodb://localhost:27017/product';

// Connexion à la base de données
mongoose.connect(url, { useNewUrlParser: true, useUnifiedTopology: true })
  .then(() => {
    console.log('Connexion à la base de données établie avec succès.');

    // Données à insérer
    const documents = [
      { name: '1 Robe blanche', description: "Ceci est la description de l'article.", price: 200, cuttedPrice: 300, images: "/img/product-1.jpg", category: "femme"},
      { name: 'jacket pour garçon', description: "Ceci est la description de l'article.", price: 250, cuttedPrice: 350, images: "/img/product-2.jpg", category: "enfant"},
      { name: 'jacket pour homme', description: "Ceci est la description de l'article.", price: 300, cuttedPrice: 400, images: "/img/product-3.jpg", category: "homme"},
      { name: 'Robe noire', description: "Ceci est la description de l'article.", price: 200, cuttedPrice: 300, images: "/img/product-4.jpg", category: "femme"},
      { name: 'tshirt pour fille', description: "Ceci est la description de l'article.", price: 100, cuttedPrice: 200, images: "/img/product-5.jpg", category: "enfant"},
      { name: 'Costume pour homme', description: "Ceci est la description de l'article.", price: 900, cuttedPrice: 1000, images: "/img/product-6.jpg", category: "homme"},
      { name: 'veste longue pour femme', description: "Ceci est la description de l'article.", price: 600, cuttedPrice: 800, images: "/img/product-7.jpg", category: "femme"},
      { name: 'chemise pour garçon', description: "Ceci est la description de l'article.", price: 200, cuttedPrice: 300, images: "/img/product-8.jpg", category: "enfant"},
      { name: 'Robe blanche', description: "Ceci est la description de l'article.", price: 200, cuttedPrice: 300, images: "/img/product-1.jpg", category: "femme"},
      
    ];

    // Insertion des documents
    Product.insertMany(documents, function(err, result) {
      if (err) {
        console.log('Erreur lors de l\'insertion des documents :', err);
      } else {
        console.log(result.length + ' documents ont été insérés dans la collection maCollection.');
      }

      // Fermeture de la connexion à la base de données
      mongoose.connection.close();
    });
  })
  .catch((err) => {
    console.log('Erreur lors de la connexion à la base de données :', err);
  });

