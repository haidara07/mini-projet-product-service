const Product = require('../models/productModel');
const asyncErrorHandler = require('../middlewares/asyncErrorHandler');

// Get All Products ---Product Sliders
exports.getProducts = asyncErrorHandler(async (req, res, next) => {
    const products = await Product.find();

    res.status(200).json({
        success: true,
        products,
    });
});

// Get Product Details
exports.getProductDetails = asyncErrorHandler(async (req, res, next) => {

    const product = await Product.findById(req.params.id);

    if (!product) {
        res.status(404).json({
            success: false,
            message:"Product Not Found",
        });
    }

    res.status(200).json({
        success: true,
        product,
    });
});