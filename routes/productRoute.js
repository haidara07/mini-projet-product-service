const express = require('express');
const { getProductDetails, getProducts} = require('../controllers/productController');

const router = express.Router();

router.route('/product').get(getProducts);
router.route('/product/:id').get(getProductDetails);

module.exports = router;